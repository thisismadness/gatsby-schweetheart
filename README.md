## Schweetheart
Schweetheart is a playground to share my personal web experiments with the world, using the newest technology and best practices. 

## Motivation
A short description of the motivation behind the creation and maintenance of the project. This should explain **why** the project exists.

## Code style
// To-do Update code guidelines [Jira issue](https://schweetheart.atlassian.net/browse/SCHWEET-7)
 
## Tech/framework used
GatsbyJS
React
Netlify
GraphQL
Prismic
NodeJS

## Features
This project uses the latest versions of NodeJS

## Installation
// To-do complete readme[Jira issue]
(https://schweetheart.atlassian.net/browse/SCHWEET-8)


## Contribute
Open a pull request of send me a message on linkedin if you have any fixes / suggestoins or improvements

##Environment variables required
PRISMIC_REPOSITORY
PRISMIC_API_KEY