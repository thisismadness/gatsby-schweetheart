require("dotenv").config();

module.exports = {
  siteMetadata: {
    title: 'Liam Schuitemaker',
    siteUrl: `https://www.schweetheart.com`,
  },
  plugins: [
    {
      resolve: "gatsby-source-prismic",
      options: {
        repositoryName: `schweetheart`,
        accessToken: `MC5YQU0zZlJBQUFDb0FYZGJn.Tu-_ve-_ve-_ve-_ve-_ve-_vWQp77-977-9Yyjvv71P77-977-9a--_ve-_vU3vv73vv71mdu-_vWF3Dz7vv73vv70`,
        linkResolver: ({ node, key, value }) => (doc) => {
          // Your link resolver
        },
        schemas: {
          // Your custom types mapped to schemas
          client: require("./src/schemas/client.json"),
          homepage: require("./src/schemas/homepage.json"),
        },
        shouldDownloadImage: ({ node, key, value }) => {
        // Return true to download the image or false to skip.
        return true
      },
      },
    },
    `gatsby-plugin-sitemap`,
    {
      resolve: 'gatsby-plugin-robots-txt',
      options: {
        policy: [{ userAgent: '*', allow: '/' }]
      }
    },
    {
      resolve: `gatsby-plugin-google-tagmanager`,
      options: {
        id: process.env.GTMID,
        includeInDevelopment: false,
      },
    },
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography.js`,
      },
    },
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Schweetheart',
        short_name: 'Schweetheart',
        start_url: '/',
        background_color: '#1f0f1e',
        theme_color: '#fd43ed',
        display: 'minimal-ui',
        icon: 'src/images/heart.png', // This path is relative to the root of the site.
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-netlify`, // make sure to put last in the array
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.app/offline
    // 'gatsby-plugin-offline',
  ],
}
