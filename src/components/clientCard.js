import React from 'react';
import { Link } from 'gatsby';
import clientCardStyles from '../components/clientCard.module.css';
import Img from 'gatsby-image';
import chevronRight from '../images/icons/chevron-right.svg';

const ClientCard = (props) => (
  <div>
    <div className="col-md-4">
      <a href={props.website}>
        <div className={clientCardStyles.clientCard}>
          <div className={clientCardStyles.content}>
            <h3>{props.name}</h3>
            <p>{props.services}</p>
            {props.website ? <a className={clientCardStyles.link} href={props.website}>View <img src={chevronRight}/></a> : null}
         </div>
         <Img fluid={props.image.localFile.childImageSharp.fluid} className={clientCardStyles.backgroundImage}/>
          <div className={clientCardStyles.gradientOverlay}>
          </div>
        </div>
      </a>
    </div>
  </div>
)

export default ClientCard;
