import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { StaticQuery, graphql } from 'gatsby';

import Header from './header';

//  Import styles that apply to all pages like the CSS grid
import './layout.css';

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <div>
        <Helmet
          title={data.site.siteMetadata.title}
          meta={[
            { name: 'og:title', content: 'Home page' },
            { name: 'og:type', content: 'website' },
            { name: 'og:url', content: 'https://www.schweetheart.com' },
            { name: 'google-site-verification', content: '-nq1S4YWCA1c2qxvzURpUZs7WN0y9DlPHCugcXiCVQs' },
          ]}
        >
          <html lang="en" />
        </Helmet>
        {/*<Header siteTitle={data.site.siteMetadata.title} />*/}
        <div>
          {children}
        </div>
      </div>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
