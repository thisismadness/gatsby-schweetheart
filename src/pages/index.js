
 import React from 'react';
import { graphql, Link } from "gatsby";
import Layout from '../components/layout';
import ClientCard from '../components/clientCard';

const IndexPage = ({data}) => (
  <Layout>
    <div className="container">
      <h1>{data.prismicHomepage.data.title.text}</h1>
      <h2>{data.prismicHomepage.data.lead.text}</h2>
      <div dangerouslySetInnerHTML={{ __html: data.prismicHomepage.data.body.html }} />
      <h2>What I've been up to lately</h2>
      <div className="row">
        {
        data.allPrismicClient.edges.map(({node}) => (
          <ClientCard 
            key={node.id} 
            name={node.data.client_name.text} 
            services={node.data.client_services} 
            image={node.data.client_image} 
            website={node.data.website_link.url} 
          />
          ))
        } 
      </div> 
    </div>
  </Layout>
)

export default IndexPage
export const query = graphql`
  query{
    allPrismicClient{
      edges{
        node {
          id
          data{
            client_name{
              text
            }
            client_services
            client_image{
              localFile{
                prettySize
                childImageSharp {
                  fluid(maxWidth:700){
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
            website_link{
              url
            }
          }
        }
      }
    }
    prismicHomepage {
    data {
      title {
        html
        text
      }
      lead {
        html
        text
      }
      body {
        html
        text
      }
    }
  }    
}`
