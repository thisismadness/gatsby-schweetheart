import Typography from "typography"
import fairyGatesTheme from 'typography-theme-fairy-gates';

fairyGatesTheme.baseFontSize = '22px' // was 20px.
fairyGatesTheme.headerColor= "#fff";
fairyGatesTheme.bodyColor= "#fff";
fairyGatesTheme.linkColor= "#fd43ed";
fairyGatesTheme.overrideThemeStyles = ({ rhythm }, options) => ({
  a: {
    color: '#fd43ed',
    textShadow: 'none',
    backgroundImage: 'linear-gradient(to top, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) 1px, #fd43ed 1px, #fd43ed 2px, rgba(0, 0, 0, 0) 2px)'
  }
});
const typography = new Typography(fairyGatesTheme);

export default typography